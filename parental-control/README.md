# Contrôle parental <!-- omit in toc -->

- [MalContent](#malcontent)
  - [Présentation](#présentation)
  - [Configuration](#configuration)
  - [Limites](#limites)
- [timekpr-next](#timekpr-next)
  - [Présentation](#présentation-1)
  - [Installation et configuration](#installation-et-configuration)
- [Firefox](#firefox)


## MalContent

### Présentation

[malcontent](https://gitlab.freedesktop.org/pwithnall/malcontent), installé par défaut sous GNOME, permet de contrôler la liste des applications autorisées.

Par défaut, il permet de configurer une liste noire d'applications à interdire.

Il permet également de définir une liste blanche avec plusieurs limites :

- :bug: la liste doit contenir tous les types MIMEs [malcontent!158](https://gitlab.freedesktop.org/pwithnall/malcontent/-/merge_requests/158)
- :window: ce mode n'est pas pris en charge par l'interface [malcontent#63](https://gitlab.freedesktop.org/pwithnall/malcontent/-/issues/63)

### Configuration

Il faut recompiler avec:

* version > 0.13.0 ou [malcontent!158](https://gitlab.freedesktop.org/pwithnall/malcontent/-/merge_requests/158) ou [malcontent!187](https://gitlab.freedesktop.org/pwithnall/malcontent/-/merge_requests/187)

La configuration est faîte avec un fichier texte par utilisateur. Par exemple pour :

* le navigateur Firefox
* la suite LibreOffice
* les *gapplications* (Cartes, Météo, ...)
* GCompris
* Calculatrice
* Tux Typing
* Brasero (Graveur CD/DVD)

```console
$ sudo cat /var/lib/AccountsService/users/basile
[com.endlessm.ParentalControls.AppFilter]
AppFilter=(true, ['/usr/lib/firefox-esr/firefox-esr', '/usr/bin/libreoffice', '/usr/bin/gapplication', '/usr/games/gcompris-qt', '/usr/bin/gnome-calculator', '/usr/games/tuxtype', '/usr/bin/brasero'])
AllowUserInstallation=false
AllowSystemInstallation=false

[User]
Session=
PasswordHint=
Icon=/usr/share/pixmaps/faces/headphones.jpg
SystemAccount=false
```

Pour que la modification de ce fichier soit prise en compte:

```console
$ sudo killall accounts-daemon
```

Vous pouvez vérifier via (nécessite [malcontent!157](https://gitlab.freedesktop.org/pwithnall/malcontent/-/merge_requests/157)):

```console
$ malcontent-client check-app-filter basile /usr/share/applications/firefox-esr.desktop 
Deskop file /usr/share/applications/firefox-esr.desktop is allowed by app filter for user basile
```

### Limites

- contournable avec Alt+F2 ([malcontent#40](https://gitlab.freedesktop.org/pwithnall/malcontent/-/issues/40))

## timekpr-next

### Présentation

[Timekpr-nExT](https://mjasnik.gitlab.io/timekpr-next/)
permet de configurer des limites de temps.

### Installation et configuration

```sh
sudo apt install -y timekpr-next
sudo adduser $USER timekpr
# logout + login
timekpra
```

## Firefox

Le fitrage internet est documenté [ici](../internet-filtering/README.md)

TODO :

* configurer
* verrouiller les paramètres
