# Configuration d'ordinateur

Ce dépôt documente plusieurs élements d'un poste de travail Linux dans un contexte familial.

* [Contrôle parental](parental-control/README.md)
* [Aides à l'écriture](writing-helpers/README.md)
* [Filtrage internet](internet-filtering/README.md)

