# Filtrage internet

## e2guardian

### Installation et configuration

```console
$ sudo apt-get install e2guardian --no-install-recommends
```

Configuration générale :

```shell
# Passage en français
sudo sed -i "s/^language =.*/language = 'french'/" /etc/e2guardian/e2guardian.conf
# Ecoute locale seulement
sudo sed -i "s/^filterip =.*/filterip = '127.0.0.1'/" /etc/e2guardian/e2guardian.conf
```

Configuration TLS MITM :

```shell
# Arborescence TLS
sudo mkdir /etc/e2guardian/private
sudo chown root:e2guardian /etc/e2guardian/private
sudo chmod 2750 /etc/e2guardian/private

# CA
sudo openssl genrsa  -out /etc/e2guardian/private/ca.key 4096
sudo openssl req -new -x509 -subj '/C=FR/L=Nantes/CN=example.org' -days 3650 -key /etc/e2guardian/private/ca.key -sha256 -out /etc/e2guardian/private/ca.pem -noenc
sudo chmod g+r /etc/e2guardian/private/ca.key

# Certificats
sudo openssl genrsa  -out /etc/e2guardian/private/cert.key 4096
sudo chmod g+r /etc/e2guardian/private/cert.key
sudo mkdir /etc/e2guardian/private/generatedcerts/
sudo chown e2guardian:e2guardian /etc/e2guardian/private/generatedcerts/
sudo chmod 2750 /etc/e2guardian/private/generatedcerts/

# Configuration TLS MiTM
sudo sed -i "s/^enablessl =.*/enablessl = on/" /etc/e2guardian/e2guardian.conf
sudo sed -i "s/^sslmitm =.*/sslmitm = on/" /etc/e2guardian/e2guardianf1.conf
```

Application de la configuration

```shell
# Relance
sudo systemctl restart e2guardian.service
```

### Configuration client

Cas général :


```shell
# Accès au CA
sudo chmod o+x /etc/e2guardian/private/
sudo ln -s /etc/e2guardian/private/ca.pem /usr/local/share/ca-certificates/
e2guardian.crt
sudo update-ca-certificates

# Env
http_proxy=http://127.0.0.1:8080
https_proxy=http://127.0.0.1:8080
```

Firefox :

```
TODO
```

![Proxy Firefox](proxy-firefox.png)

* https://support.mozilla.org/en-US/kb/customizing-firefox-using-autoconfig
* https://mozilla.github.io/policy-templates/
