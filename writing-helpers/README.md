# Aides à l'écriture

## ibus-typing-booster

### Présentation

[ibus-typing-booster](https://mike-fabian.github.io/ibus-typing-booster/) permet de saisir plus rapidement.

### Installation

```console
$ sudo apt install ibus-typing-booster
```

Il faut ensuite quitter et rouvrir la session.

### Configuration

Suivre [la documentation pour l'installation](https://mike-fabian.github.io/ibus-typing-booster/docs/user/#1_1).

Voici les paramètres que je conseille :

* Options :
  * [x] Disable in terminals
  * [x] Mode sans enregistrement :question:
  * Nombre minimum de caractères pour le complément : `3`
* Apparence :
  * [x] Utiliser un libellé pour les suggestions provenant de la base de données utilisateur : `⭐`
  * [x] Utiliser un libellé pour les suggestions orthographiques : `✓`
  * [x] Utiliser un libellé pour les suggestions du dictionnaire : `📖`

### Limites

* *[ENHANCEMENT] Record frequency on correctly spelled words only* [ibus-typing-booster#470](https://github.com/mike-fabian/ibus-typing-booster/issues/470)
